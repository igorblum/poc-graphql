FROM maven:3-jdk-8

COPY . /app
WORKDIR /app

#RUN mvn package

CMD [ "java","-jar", "target/graphql-poc.jar" ]
EXPOSE 8888